<?php

namespace Drupal\d01_drupal_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class D01DrupalWebformSettingsAssets
 *
 * @package Drupal\d01_drupal_webform\Form
 */
class D01DrupalWebformSettingsAssets extends ConfigFormBase {

  const CONFIG_NAME = 'd01_drupal_webform.settings_assets';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_webform_settings_assets';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      D01DrupalWebformSettingsAssets::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config(D01DrupalWebformSettingsAssets::CONFIG_NAME);
    $default = $config->get('settings');

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['settings']['disabled_functionalities'] = [
      '#title' => $this->t('Disabled functionalities'),
      '#type' => 'checkboxes',
      '#options' => [
        'css' => t('Custom CSS'),
        'js' => t('Custom Javascript'),
      ],
      '#default_value' => isset($default['disabled_functionalities']) ? $default['disabled_functionalities'] : [],
      '#description' => t('Check the functionalities you want to disable. When none are checked all functionalities will be considered as enabled.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(D01DrupalWebformSettingsAssets::CONFIG_NAME);
    $config->set('settings', $form_state->getValue('settings'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
