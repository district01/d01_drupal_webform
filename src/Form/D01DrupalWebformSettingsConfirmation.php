<?php

namespace Drupal\d01_drupal_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;

/**
 * Class D01DrupalWebformSettingsConfirmation
 *
 * @package Drupal\d01_drupal_webform\Form
 */
class D01DrupalWebformSettingsConfirmation extends ConfigFormBase {

  const CONFIG_NAME = 'd01_drupal_webform.settings_confirmation';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_webform_settings_confirmation';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      D01DrupalWebformSettingsConfirmation::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config(D01DrupalWebformSettingsConfirmation::CONFIG_NAME);
    $default = $config->get('settings');

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['settings']['confirmation_type'] = [
      '#title' => $this->t('Confirmation type'),
      '#type' => 'checkboxes',
      '#options' => [
        WebformInterface::CONFIRMATION_PAGE => WebformInterface::CONFIRMATION_PAGE,
        WebformInterface::CONFIRMATION_INLINE => WebformInterface::CONFIRMATION_INLINE,
        WebformInterface::CONFIRMATION_MESSAGE => WebformInterface::CONFIRMATION_MESSAGE,
        WebformInterface::CONFIRMATION_MODAL => WebformInterface::CONFIRMATION_MODAL,
        WebformInterface::CONFIRMATION_URL => WebformInterface::CONFIRMATION_URL,
        WebformInterface::CONFIRMATION_URL_MESSAGE => WebformInterface::CONFIRMATION_URL_MESSAGE,
      ],
      '#default_value' => isset($default['confirmation_type']) ? $default['confirmation_type'] : [],
      '#description' => t('Check the confirmation types you want to support. When none are checked all types will be considered as supported.'),
    ];

    $form['settings']['disabled_functionalities'] = [
      '#title' => $this->t('Disabled functionalities'),
      '#type' => 'checkboxes',
      '#options' => [
        'custom_message' => t('Custom message'),
        'attributes' => t('Attributes'),
        'back_button' => t('Back button'),
      ],
      '#default_value' => isset($default['disabled_functionalities']) ? $default['disabled_functionalities'] : [],
      '#description' => t('Check the functionalities you want to disable. When none are checked all functionalities will be considered as enabled.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(D01DrupalWebformSettingsConfirmation::CONFIG_NAME);
    $config->set('settings', $form_state->getValue('settings'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
