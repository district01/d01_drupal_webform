<?php

namespace Drupal\d01_drupal_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Plugin\WebformElementManager;
use Drupal\webform_ui\Form\WebformUiElementFormBase;
use Drupal\Core\Form\FormState;

/**
 * Class D01DrupalWebformBuildElements.
 *
 * @package Drupal\d01_drupal_webform\Form
 */
class D01DrupalWebformBuildElements extends ConfigFormBase {

  const CONFIG_NAME = 'd01_drupal_webform.build_elements';

  /**
   * Webform element.
   *
   * @var \Drupal\webform\Plugin\WebformElementInterface
   */
  protected $webformElementManager;

  /**
   * Constructor.
   *
   * @param \Drupal\webform\Plugin\WebformElementManager $webform_element_manager
   *   Webform element plugin.
   */
  public function __construct(WebformElementManager $webform_element_manager) {
    $this->webformElementManager = $webform_element_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.webform.element')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_webform_build_elements';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      D01DrupalWebformBuildElements::CONFIG_NAME,
    ];
  }

  /**
   * Get the webform components.
   *
   * @return array
   *   A keyed array of components.
   */
  private function getComponents() {
    $components = [];
    $plugin_definitions = $this->webformElementManager->getDefinitions();
    $plugin_definitions = $this->webformElementManager->getSortedDefinitions($plugin_definitions);

    // Initialize and return all plugin instances.
    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      $instance = $this->webformElementManager->createInstance($plugin_id);
      $props = $this->getComponentDefaultProperties($instance);
      $props['custom'] = 'custom';
      $components[$plugin_id] = $props;
    }

    ksort($components);
    return $components;
  }

  /**
   * Get the default properties of a webform element.
   *
   * @param \Drupal\webform\Plugin\WebformElementInterface $component
   *   A webform element.
   *
   * @return array
   *   A keyed array of properties.
   */
  private function getComponentDefaultProperties(WebformElementInterface $component) {
    $properties = [];
    $default_properties = $component->getDefaultProperties();
    foreach ($default_properties as $key => $value) {
      $properties[$key] = $key;
    }
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config(D01DrupalWebformBuildElements::CONFIG_NAME);
    $default = $config->get('elements');

    $form['description'] = [
      '#title' => t('Elements'),
      '#type' => 'item',
      '#markup' => t('Check the properties you want to support for each webform element. When no properties are checked for a component all properties will be considered as supported.'),
    ];

    $form['elements'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $components = $this->getComponents();
    if (!empty($components)) {
      foreach ($components as $component => $options) {
        $form['elements'][$component] = [
          '#type' => 'details',
          '#title' => $component,
          '#collapsed' => TRUE,
          '#collapsible' => TRUE,
        ];

        $form['elements'][$component]['properties'] = [
          '#type' => 'checkboxes',
          '#options' => $options,
          '#default_value' => isset($default[$component]['properties']) ? $default[$component]['properties'] : [],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(D01DrupalWebformBuildElements::CONFIG_NAME);
    $config->set('elements', $form_state->getValue('elements'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
