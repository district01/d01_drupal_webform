<?php

namespace Drupal\d01_drupal_webform;

use Drupal\webform\WebformEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of webform entities.
 *
 * @see \Drupal\webform\Entity\Webform
 */
class D01DrupalWebformListBuilder extends WebformEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $user = \Drupal::currentUser();
    $row = parent::buildRow($entity);

    if (!$user->hasPermission('access webform build')) {
      unset($row['operations']['data']['#links']['build']);
    }

    if (!$user->hasPermission('access webform test')) {
      unset($row['operations']['data']['#links']['test']);
    }

    if (!$user->hasPermission('access webform general settings')) {
      unset($row['operations']['data']['#links']['settings']);
    }

    if (!$user->hasPermission('access webform duplicate')) {
      unset($row['operations']['data']['#links']['duplicate']);
    }

    return $row;
  }
}
