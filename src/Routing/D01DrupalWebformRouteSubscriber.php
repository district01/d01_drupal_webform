<?php

namespace Drupal\d01_drupal_webform\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Finetune permissions for webforms.
 */
class D01DrupalWebformRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Fine tune general permissions for webforms.
    foreach ($this->generalRoutePermissionMapping() as $route_path => $permission) {
      if ($collection->get($route_path)) {
        $route = $collection->get($route_path);
        $route->setRequirement('_permission', $permission);
      }
    }

    // Fine tune permissions for specific webform.
    foreach ($this->webformRoutePermissionMapping() as $route_path => $permission) {
      if ($collection->get($route_path)) {
        $route = $collection->get($route_path);
        $route->setRequirement('_permission', $permission);
      }
    }

    // Remove commercial about page from webform module.
    $routes = ['webform.about', 'webform.about.drupal'];
    foreach ($routes as $route_path) {
      if ($collection->get($route_path)) {
        $route = $collection->get($route_path);
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

  /**
   * Map of route and permission.
   *
   * @return array
   *   an array of permissions keyed by route.
   */
  private function generalRoutePermissionMapping() {
    return [
      'webform.settings' => 'access general webform settings',
      'webform.element_plugins' => 'access general webform plugins',
      'webform.addons' => 'access general webform addons',
    ];
  }

  /**
   * Map of route and permission.
   *
   * @return array
   *   an array of permissions keyed by route.
   */
  private function webformRoutePermissionMapping() {
    return [
      'entity.webform.settings' => 'access webform general settings',
      'entity.webform.settings_form' => 'access webform form settings',
      'entity.webform.settings_submissions' => 'access webform submissions settings',
      'entity.webform.settings_confirmation' => 'access webform confirmation settings',
      'entity.webform.handlers' => 'access webform handlers settings',
      'entity.webform.settings_assets' => 'access webform assets settings',
      'entity.webform.settings_access' => 'access webform access settings',
      'entity.webform.test_form' => 'access webform test',
      'entity.webform.edit_form' => 'access webform build',
      'entity.webform.source_form' => 'access webform build source',
      'entity.webform.results_submissions' => 'access webform results',
      'entity.webform.results_export' => 'access webform export',
      'entity.webform.results_clear' => 'access clear webform results',
    ];
  }

}
